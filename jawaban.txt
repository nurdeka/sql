cd c:\xampp\mysql\bin
cmd : mysql -uroot -p

1. Membuat database
create database myshop;

2. membuat tabel
create table categories(
    -> id int(8) auto_increment,
    -> name varchar(255),
    -> primary key(id)
    -> );
create table items(
    -> id int(8) auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price integer,
    -> stock integer,
    -> category_id integer,
    -> primary key(id),
    -> foreign key(category_id) references categories(id)
    -> );
create table users(
    -> id int(8) auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255),
    -> primary key(id)
    -> );

3. memasukkan data pada tabel
insert into categories(name) values("gadget"),("cloth"),("men"),("women"),("branded");
insert into items(name, description, price, stock, category_id) values(
    -> "Sumsang b50", "hape keren dari sumsang", 4000000, 100, 1),("unikloh", "baju keren dari brand ternama", 500000, 50, 2),("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);
insert into users(name, email, password) values(
    -> "John Doe", "john@doe.com", "john123"),("Jane Doe", "jane@doe.com", "jenita123");

4. mengambil data dari database
a. mengambil data users
select id, name, email from users;

b. mengambil data items
select * from items where price > 1000000;

select * from items where name like '%sang%';

c. Menampilkan data items join dengan kategori
ALTER TABLE categories CHANGE name category varchar(255);
select items.name, items.description, items.price, items.stock, items.category_id, categories.category from items inner join categories on items.category_id = categories.id;

5. mengubah data dari database
update items
    -> set price = 2500000
    -> where name like '%sang%';




